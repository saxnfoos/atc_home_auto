define( 
	"homeautocontroller",

	[ "jquery" ],

	function( $ ) {
		"use strict";

		var HomeController = function( options ) {
			this.namespace = options.namespace || "GenericHomeAutoController";

			this.light = options.light || null;
			
			this.curtains = options.curtains || null;

			this.thermostat = options.thermostat || null;

			this.tempDownButton = options.downButton || $( "#buttonDown" );

			this.tempUpButton = options.upButtom || $( "#buttonUp" );
	
			this.lightSwitch = options.lightSwitch || $( "#lightSwitch" );

			this.curtainControl = options.curtainControl || $( "#curtainControl" );
			
			//set listeners
			this.tempUpButton.on( "click", this.setTemp.bind( this, "+") );
	
			this.tempDownButton.on( "click", this.setTemp.bind( this, "-") );

			this.lightSwitch.on( "click", this.toggleLight.bind( this ) );

			this.curtainControl.on( "click", this.toggleCurtains.bind( this ) );
		}

		/* incrementally increase or decrease temparature in the home */
		HomeController.prototype.setTemp = function( direction ) {
			this.thermostat.setTemp( direction );
		};

		HomeController.prototype.toggleLight = function( ) {
			this.light.toggleLight();
		};

		HomeController.prototype.toggleCurtains = function() {
			this.curtains.toggleCurtains();
		};

		return HomeController;
	}
);

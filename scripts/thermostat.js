define( 
	"thermostat",

	[ "jquery" ],

	function( $ ) {
		"use strict";

		var STATIC_DIR_DOWN = "-",
			STATIC_DIR_UP = "+";
		
		var Thermostat = function( options ) {
			this.tempDisplay = options.tempDisplay;
		};

		/*
		 * API call to grab current temp in location
		 */
		Thermostat.prototype.getCurrentTemp = function() {
			$.ajax( "api/temp.json", {
					context: this,
					method: "GET"
				} )
				.done( function( resp ) {
					if ( !resp[0] || !resp[0].currentConditions || !resp[0].currentConditions.currentTemp ) {
						return;
					}

					this.currentTemp = resp[0].currentConditions.currentTemp; 

					this.tempDisplay.val ( this.currentTemp );
				} )
				.fail( function() {
					console.log( "error getting current temperature" );
				} ) ;
		};

		/*
		 * API call to set current temp in location
		 */
		Thermostat.prototype.setTemp = function( direction ) {
			var t = direction === STATIC_DIR_UP ? this.currentTemp + 1 : this.currentTemp - 1;

			//add limit
			if ( t > 90 ) {
				t = 90;
			} else if ( t < 32 ) {
				t = 32;
			}

			//currently executes call that returns static json, passing in new temp to set;
			$.ajax( "api/updatetemp.json", {
					context: this,
					data: {
						"temp": t
					},
					method: "POST"
				} )
				.done( function( resp ) {
					if ( !resp[0] ) {
						return;
					}

					this.currentTemp = t;

					this.tempDisplay.val ( this.currentTemp );
				} )
				.fail( function() {
					console.log( "error getting current temperature" );
				} ) ;
		}

		return Thermostat;
	}
);

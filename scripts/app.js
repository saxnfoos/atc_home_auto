requirejs.config( {
    "paths": {
      "jquery": "//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min",
      "homeautocontroller": "controller",
      "light": "light",
      "curtains": "curtains",
      "thermostat": "thermostat"
    }
} );

requirejs( [ "main" ] );
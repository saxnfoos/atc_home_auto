/* 
 *	This is a simple home automation control app that interfaces with:
 *	- curtains
 *	- thermostat
 *	- lightswitch
 */

require( [ 
	"jquery",
 	"homeautocontroller",
 	"light",
 	"curtains",
 	"thermostat"
],

function( $, HomeController, Light, Curtains, Thermostat ) {
	"use strict";

	var light = new Light( {
			"lightblock": $( "#lightBlock" )
		} ), 
		
		curtains = new Curtains( {
			"curtainset": $( "#curtainSet")
		}),
		
		thermostat = new Thermostat( {
			"tempDisplay": $( "#currentTempDisplay" )
		} );


	//instantiate instance of HomeController
	var controller = new HomeController( {
		light: light,
		curtains: curtains,
		thermostat: thermostat
	} );

	//set temp display to current temp in home
	thermostat.getCurrentTemp();
} );

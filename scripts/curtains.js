define( 
	"curtains",

	[ "jquery" ],

	function( $ ) {
		"use strict";
		
		var STATE_CLOSED = 1,
			STATE_OPENED = 0;

		var Curtains = function( options ) {
			this.curtainset = options.curtainset || null ;
			
			this.state = STATE_CLOSED;

			this.openedCss = "open"
		};

		//toggle curtains opened or closed based on current state of the curtains
		Curtains.prototype.toggleCurtains = function(  ) {
			if ( !this.state ) {
				$( this.curtainset ).addClass( this.openedCss );
			} else {
				$( this.curtainset ).removeClass( this.openedCss );
			}

			this.state = !this.state;
		};

		return Curtains;
	}
);

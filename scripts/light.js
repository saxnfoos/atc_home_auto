define( 
	"light",

	[ "jquery" ],

	function( $ ) {
		"use strict";
		
		var STATE_ON = 1,
			STATE_OFF = 0;

		var Light = function( options ) {
			this.lightblock = options.lightblock || null ;
			
			this.state = STATE_OFF;

			this.onCss = "on"
		};

		//toggle a light on or off based on current state of the light
		Light.prototype.toggleLight = function(  ) {
			if ( !this.state ) {
				$( this.lightblock ).addClass( this.onCss );
			} else {
				$( this.lightblock ).removeClass( this.onCss );
			}

			this.state = !this.state;
		};

		return Light;
	}
);
